PVector position;
PVector vitesse;
int delta = 2;

void setup() {
  size(400, 500);
  position = new PVector(width/2, height/2); //on initialise la position au centre du sketch
  vitesse = new PVector(0, 0);
}

void draw() {
  background(0);

  ellipse(position.x, position.y, 10, 10);
  position.add(vitesse);

  //Variation d'angle du déplacement ("Random walker")
  PVector angle = PVector.fromAngle(random(TWO_PI)); // Cercle trigonométrique
  vitesse.limit(delta); // On limite la vitesse poue ne pas accelerer indéfiniment
  vitesse.add(angle);
  // Contrainte du cercle aux limites du sketch
  if (position.x < 0 || position.x > width) {
    vitesse.x *= -1;
  }

  if (position.y < 0 || position.y > height) {
    vitesse.y *= -1;
  }
}

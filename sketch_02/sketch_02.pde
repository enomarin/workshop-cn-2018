int posX, posY;

void setup() {
  size(500, 500);
  posX = 0;
  posY = height/2;
}

void draw() {
  ellipse(posX, posY, 10, 10);
  posX = posX + 1; // equivalences
  //posX += 1;
  //posX++;
}

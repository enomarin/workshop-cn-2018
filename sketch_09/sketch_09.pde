PVector position;
PVector vitesse;

float p = 0.01;
void setup() {
  size(400, 500);
  position = new PVector(width/2, height/2); //on initialise la position au centre du sketch
  vitesse = new PVector(1, 1);
}

void draw() {
  background(0);

  float colorValue = map(noise(p), 0, 1, 0, 255);
  //color c = color(colorValue);
  color c = color(colorValue, 0, 0);

  p += 0.05;

  fill(c);
  ellipse(position.x, position.y, 10, 10);
  position.add(vitesse);

  // Contrainte du cercle aux limites du sketch
  if (position.x < 0 || position.x > width) {
    vitesse.x *= -1;
  }

  if (position.y < 0 || position.y > height) {
    vitesse.y *= -1;
  }
}

class Cercle {
  PVector position;
  PVector vitesse;

  Cercle(int initX, int initY) {
    position = new PVector(initX, initY);
    vitesse = new PVector(1, 1);
  }

  // display : affiche le cercle à l'écran, ne renvoie rien
  void display() {
    ellipse(position.x, position.y, 10, 10);
  }
  // Fonction de mise à jour de la position du cercle
  void update() {
    move();
    checkEdge();
  }
  // move : fait bouger le cercle à l'écran, on ajoute l'ensemble des forces à la position
  void move() {
    position.add(vitesse);
  }

  void checkEdge() {
    // Contrainte du cercle aux limites du sketch
    if (position.x < 0 || position.x > width) {
      vitesse.x *= -1;
    }

    if (position.y < 0 || position.y > height) {
      vitesse.y *= -1;
    }
  }
}

class Cercle {
  int diametre = 1;
  PVector position;
  PVector vitesse;
  int delta = 2;
  float p;
  Cercle(int initX, int initY) {
    position = new PVector(initX, initY);
    vitesse = new PVector(1, 1);
    p = random(666);
  }

  // display : affiche le cercle à l'écran, ne renvoie rien
  void display() {
    float colorValue = map(noise(p), 0, 1, 0, 255);
    //color c = color(colorValue);
    color c = color(colorValue, 0, 0);

    p += 0.05;
    
    noStroke();
    fill(c);
    ellipse(position.x, position.y, diametre, diametre);
  }
  // Fonction de mise à jour de la position du cercle
  void update() {
    move();
    checkEdge();
  }
  // move : fait bouger le cercle à l'écran, on ajoute l'ensemble des forces à la position
  void move() {
    vitesse.add(hesitation());
    vitesse.limit(delta);
    position.add(vitesse);
  }
  PVector hesitation() {
    //Variation d'angle du déplacement ("Random walker")
    PVector angleVector = PVector.fromAngle(random(TWO_PI)); // Cercle trigonométrique
    return angleVector;
  }
  void checkEdge() {
    // Contrainte du cercle aux limites du sketch
    if (position.x < 0 || position.x > width) {
      vitesse.x *= -1;
    }

    if (position.y < 0 || position.y > height) {
      vitesse.y *= -1;
    }
  }
}

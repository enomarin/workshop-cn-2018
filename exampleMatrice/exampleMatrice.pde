/*
Comment parcourir la matrice de pixel ?
 
 On parcours la matrice en x et en y avec deux boucles imbriquées
 On définit pour chaques pixels une couleur en niveau de gris aléatoire entre 0 et 255
 */

void setup() {
  size(500, 500);
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      stroke(random(255));
      point(x, y);
    }
  }
}

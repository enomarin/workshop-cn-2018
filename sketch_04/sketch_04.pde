int posX, posY;
int delta = 2;

void setup() {
  size(500, 500);
  posX = 0;
  posY = height/2;
}

void draw() {
  background(0);
  ellipse(posX, posY, 10, 10);
  posX += delta;

/*
  if (posX > width) {
    //expliquer différence avec delta = -1 => utilité du paramètre
    // delta *= -1 est équivalent à delta = delta * -1
    delta *= -1; 
  }

  if (posX < 0) {
    delta *= -1;
  }
*/

  if (posX < 0 || posX > width) {
    delta *= -1;
  }

}

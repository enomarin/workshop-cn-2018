/*
Comment dessiner du texte dans Processing ?
*/

void setup() {
  size(500, 500);
  background(0);
  textAlign(CENTER); // aligne le texte au centre
  text("hello world", width/2, height/2);
}
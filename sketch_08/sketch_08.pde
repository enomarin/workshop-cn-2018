PVector position;
PVector vitesse;

float p = 0.01;
void setup() {
  size(400, 500);
  position = new PVector(width/2, height/2); //on initialise la position au centre du sketch
  vitesse = new PVector(1, 1);
}

void draw() {
  background(0);

  ellipse(position.x, position.y, 10, 10);
  position.add(vitesse);

  // Variation de la vitesse
  // Vitesse.setMag(random(5));
  // Vitesse.setMag(noise(p));

  float deltaVitesse = map(noise(p), 0, 1, 0, 5);
  vitesse.setMag(deltaVitesse);
  p += 0.05;

  // Contrainte du cercle aux limites du sketch
  if (position.x < 0 || position.x > width) {
    vitesse.x *= -1;
  }

  if (position.y < 0 || position.y > height) {
    vitesse.y *= -1;
  }
}
int posX, posY;

void setup() {
  size(500,500);
  posX = 0;
  posY = height/2;
}

void draw() {
  background(0);
  ellipse(posX, posY, 10, 10);
  posX += 1;
  
  if(posX > width) {
    posX = 0;
  }
}

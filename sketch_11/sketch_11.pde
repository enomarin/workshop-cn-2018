Cercle cercle;

void setup() {
  size(500, 500);
  cercle = new Cercle(width/2, height/2);
}

void draw() {
  background(0);
  cercle.update();
  cercle.display();
}

class Cercle {
  int diametre = 5;
  PVector position;
  PVector vitesse;
  int delta = 2;
  
  Cercle(int initX, int initY) {
    position = new PVector(initX, initY);
    vitesse = new PVector(1, 1);
  }

  // display : affiche le cercle à l'écran, ne renvoie rien
  void display() {
    noStroke();
    fill(255);
    ellipse(position.x, position.y, diametre, diametre);
  }
  // Fonction de mise à jour de la position du cercle
  void update() {
    move();
    checkEdge();
  }
  // move : fait bouger le cercle à l'écran, on ajoute l'ensemble des forces à la position
  void move() {
    vitesse.add(hesitation());
    vitesse.limit(delta);
    position.add(vitesse);
  }
  PVector hesitation() {
    //Variation d'angle du déplacement ("Random walker")
    PVector angleVector = PVector.fromAngle(random(TWO_PI)); // Cercle trigonométrique
    return angleVector;
  }
  void checkEdge() {
    // Contrainte du cercle aux limites du sketch
    if (position.x < 0 || position.x > width) {
      vitesse.x *= -1;
    }

    if (position.y < 0 || position.y > height) {
      vitesse.y *= -1;
    }
  }
}

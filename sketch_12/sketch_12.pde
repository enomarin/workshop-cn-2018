Cercle[] cercles;
int nbeCercles = 100;
void setup() {
  size(500, 500);
  cercles = new Cercle[nbeCercles];
  for (int index = 0; index < nbeCercles; index ++) {
    // On instancie des cercles à des positions aléatoires
    cercles[index] = new Cercle(int(random(width)), int(random(height)));
  }
}

void draw() {
  fill(0, 10);
  rect(0, 0, width, height);
  for (int index = 0; index < nbeCercles; index ++) {
    cercles[index].update();
    cercles[index].display();
  }
}

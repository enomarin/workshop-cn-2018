int posX, posY;

void setup() {
  size(500,500);
  posX = width/2;
  posY = height/2;
}

void draw() {
  ellipse(posX, posY, 10, 10);
}

PVector position;
PVector vitesse;
int delta = 5;

void setup() {
  size(400, 500); // essayer plusieurs taille de sketch
  position = new PVector(width/2, height/2); //on initialise la position au centre du sketch
  vitesse = new PVector(random(-delta, delta), random(-delta, delta));
}

void draw() {
  background(0);
  ellipse(position.x, position.y, 10, 10);
  position.add(vitesse);
  
  if (position.x < 0 || position.x > width) {
    vitesse.x *= -1;
  }

  if (position.y < 0 || position.y > height) {
    vitesse.y *= -1;
  }
}

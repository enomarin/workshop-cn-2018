int posX, posY;
int vitesse = 2;
int deltaX, deltaY;

void setup() {
  size(400, 500); // essayer plusieurs taille de sketch
  posX = 0;
  posY = height/2;
  deltaX = vitesse; // Q? comment donner au point une direction aléatoire ?
  deltaY = vitesse;
}

void draw() {
  background(0);
  ellipse(posX, posY, 10, 10);
  posX += deltaX;
  posY += deltaY;
  
  if (posX < 0 || posX > width) {
    deltaX *= -1;
  }
  
  if (posY < 0 || posY > height) {
    deltaY *= -1;
  }
}
